import json
from import_manager import ImportManager

with open("config.json") as f:
    data = json.load(f)

    im = ImportManager()
    im.load_data_from_json(data)

    im.run()



# im = ImportManager()
