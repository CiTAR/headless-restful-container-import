# Upload container

Library for uploading container image to CITAR backend-server with/-out handling.

## Getting Started

How to run this script at HPC (as bwUniCluster).

### Prerequisites

We need to create virtual environment for python project. Command *virualenv* should be available at your HPC. You can read more about **Virtualenv** at https://docs.python-guide.org/dev/virtualenvs/

```
$ virtualenv venv
```

### Installing

1. Have to download script. For this can be used git or you can do it by your ways.

```
$ git clone https://gitlab.com/CiTAR/headless-restful-container-import.git
```

2. Install libraries

```
$ source venv/bin/activate
$ pip install -r headless-restful-container-import/requirements.txt
```

After this script can be used.

## Running

1. For running should be used our virtual environment. It should be activate To use it run next command:
```
$ source venv/bin/activate
```
You should see something like this:
```
(venv) $

```
2. Enter directory with script:
```
(venv) $ cd headless-restful-container-import
```

3. Fill the config file **config.json** using your favorite editor. You can find it on directory with script.
```
(venv) $ vi config.json
```

4. Run script file.
```
(venv) $ python upload.py
```
5. Run next command to deactivate virtualenv:
```
(venv) $ deactivate
```

## Additional info
1. File *howto* contain guide how to import from docker to singulatiry.
